package com.example;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.microsoft.aad.msal4j.ClientCredentialFactory;
import com.microsoft.aad.msal4j.ClientCredentialParameters;
import com.microsoft.aad.msal4j.ConfidentialClientApplication;
import com.microsoft.aad.msal4j.IAuthenticationResult;
import com.microsoft.aad.msal4j.IClientCredential;
import com.microsoft.sqlserver.jdbc.SQLServerDataSource;
import java.security.SecureRandom;
/**
 * Hello world!
 *
 */
public class App {
    public static void execute(){
    

        try {
            SecureRandom random = new SecureRandom();
            byte bytes[] = new byte[20];
            random.nextBytes(bytes);
            //System.out.println ("s2" + byteArrayToHexString(bytes));

            String spn = "https://database.windows.net/";
            String stsurl = "https://login.microsoftonline.com/513294a0-3e20-41b2-a970-6d30bf1546fa/oauth2/v2.0/token"; // Replace with your STS URL.
            String clientId = "0e410f43-e3ad-443f-9e9e-c8d46dd87602"; // Replace with your client ID.
            String clientSecret = "XlU7Q~RSNQrAUNE-i2mGvETWLTN5.YLkZe2qA"; // Replace with your client secret.
            //secret id - 81f5434f-3d7b-4900-886e-026ab9300e3f

            String scope = spn +  "/.default";
            Set<String> scopes = new HashSet<String>();
            scopes.add(scope);

            ExecutorService executorService = Executors.newSingleThreadExecutor();
            IClientCredential credential = ClientCredentialFactory.createFromSecret(clientSecret);
            ConfidentialClientApplication clientApplication = ConfidentialClientApplication
                .builder(clientId, credential).executorService(executorService).authority(stsurl).build();
            CompletableFuture<IAuthenticationResult> future = clientApplication
                .acquireToken(ClientCredentialParameters.builder(scopes).build());

            IAuthenticationResult authenticationResult = future.get();
            String accessToken = authenticationResult.accessToken();

            System.out.println("Access Token: " + accessToken);


            // String accessToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIng1dCI6Imwzc1EtNTBjQ0g0eEJWWkxIVEd3blNSNzY4MCIsImtpZCI6Imwzc1EtNTBjQ0g0eEJWWkxIVEd3blNSNzY4MCJ9.eyJhdWQiOiJodHRwczovL2RhdGFiYXNlLndpbmRvd3MubmV0LyIsImlzcyI6Imh0dHBzOi8vc3RzLndpbmRvd3MubmV0LzUxMzI5NGEwLTNlMjAtNDFiMi1hOTcwLTZkMzBiZjE1NDZmYS8iLCJpYXQiOjE2MzY5NjkzMjcsIm5iZiI6MTYzNjk2OTMyNywiZXhwIjoxNjM2OTczMjI3LCJhaW8iOiJFMlpnWUZobitjRDYxRkgyb0o4Ni9GdGFSUCtJQUFBPSIsImFwcGlkIjoiMGU0MTBmNDMtZTNhZC00NDNmLTllOWUtYzhkNDZkZDg3NjAyIiwiYXBwaWRhY3IiOiIxIiwiaWRwIjoiaHR0cHM6Ly9zdHMud2luZG93cy5uZXQvNTEzMjk0YTAtM2UyMC00MWIyLWE5NzAtNmQzMGJmMTU0NmZhLyIsImlkdHlwIjoiYXBwIiwib2lkIjoiOTk4ZGFlYjgtZTQ1Ny00M2MyLTk1NDctMDdhMWU0NDZlZTIwIiwicmgiOiIwLkFUZ0FvSlF5VVNBLXNrR3BjRzB3dnhWRy1rTVBRUTZ0NHo5RW5wN0kxRzNZZGdJNEFBQS4iLCJzdWIiOiI5OThkYWViOC1lNDU3LTQzYzItOTU0Ny0wN2ExZTQ0NmVlMjAiLCJ0aWQiOiI1MTMyOTRhMC0zZTIwLTQxYjItYTk3MC02ZDMwYmYxNTQ2ZmEiLCJ1dGkiOiJ3b1p0WF9wQlBVaUphM2JqRElnNEFBIiwidmVyIjoiMS4wIn0.o0_XmKbrVUUdQUwcmKbPur1Pnqnpba5e3AuTXlEOX0EsQA2cbJ48zL7_v_Uxkrf77NI--9HxLA2vynfR7x1zJgT-eutQ8hAZdEzJ7b3WMgp-8MvZnlvzWS2ujO_QscrhWkpaUgze4nx6P0ssfWLOesJ7wI3nQvbkAbGOxZNp_fhEg6oH7lG-3taBtkO13fkoa904PkSRaJPdMpk0I2OIFFJUc7kIeOwBxshfVh15WsyEPmUKJjrYzNvRBIZNNZTIWhbVadxJSnu9FWPtvGsn25NVwaI0Mfsa8kWm5yKsvzoPS-WmVSIJxD1gIwkxVBDGhXXKRIp6OLpJ8Eqj2p5aFA";
            
            // Connect with the access token using MS Class.
            //
            SQLServerDataSource ds = new SQLServerDataSource();

            ds.setServerName("rondinidb.database.windows.net"); // Replace with your server name.
            ds.setDatabaseName("saxoncs"); // Replace with your database name.
            ds.setAccessToken(accessToken);

            try (Connection connection = ds.getConnection();
                    Statement stmt = connection.createStatement();
                    ResultSet rs = stmt.executeQuery("SELECT * from saxoncs.internaluser")) {
                
                System.out.println("----------------");
                while (rs.next()) {
                    System.out.println(" MS JDBC Connection resultset: " + rs.getString(1));
                }
            }
            
            // Connect to SQL using standard JDBC class
            //
            String jdbcURL = "jdbc:sqlserver://rondinidb.database.windows.net:1433;database=saxoncs";
            Properties properties = new Properties();
            properties.put("accessToken", accessToken);

            try (
                Connection connection = DriverManager.getConnection(jdbcURL, properties);
                Statement stmt2 = connection.createStatement();
                ResultSet rs2 = stmt2.executeQuery("SELECT * from saxoncs.internaluser")) {

                System.out.println("----------------");
                while (rs2.next()) {
                    System.out.println(" Standard JDBC Connection resultset: " + rs2.getString(1));
                }
            }

        } catch  (Exception e ){
            e.printStackTrace();
        }
    }

    public static String byteArrayToHexString(byte[] byteArray) {
        int readBytes = byteArray.length;
        StringBuffer hexData = new StringBuffer();
        int onebyte;
        for (int i=0; i < readBytes; i++) {
            onebyte = ((0x000000ff & byteArray[i]) | 0xffffff00);
            hexData.append(Integer.toHexString(onebyte).substring(6));
        }
        return hexData.toString();
    }
}
